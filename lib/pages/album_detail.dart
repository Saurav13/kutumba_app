import 'package:flutter/material.dart';

import './../main_drawer.dart';
import './../components/header_logo.dart';

class AlbumDetail extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AlbumDetail();
  }
}

class _AlbumDetail extends State<AlbumDetail> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.black12,
      endDrawer: MainDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.black12,
        // Here we take the value from the MyAlbumsPage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: HeaderLogo(),
        centerTitle: false,
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              
                Container(
                  height: 400,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    image: const DecorationImage(
                      image: AssetImage('assets/images/album1.jpg'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(height: 15),
                      Text('NAULO BIHANI',
                          style: TextStyle(
                              color: Color.fromARGB(255, 175, 175, 175),
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              letterSpacing: 5)),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text('Artist:',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 175, 175, 175),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 4)),
                          ),
                          Expanded(
                            flex: 3,
                            child: Text('Kutumba',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 175, 175, 175),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 4)),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text('Release Date:',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 175, 175, 175),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 4)),
                          ),
                          Expanded(
                            flex: 3,
                            child: Text('2009-09-09',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 175, 175, 175),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 4)),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text('Label:',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 175, 175, 175),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 4)),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Text('10 tracks',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 175, 175, 175),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 4)),
                          ),
                          Expanded(
                            flex: 3,
                            child: Text('1 hr 06 mins 53 secs',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 175, 175, 175),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 4)),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
