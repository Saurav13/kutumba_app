import 'package:flutter/material.dart';

import './../main_drawer.dart';
import './../components/header_logo.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Home();
  }
}

class _Home extends State<Home> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.black12,
      endDrawer: MainDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.black12,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: HeaderLogo(),
        centerTitle: false,
      ),
      body: ListView(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset('assets/images/img1.png'),
              Padding(
                padding: const EdgeInsets.fromLTRB(20,0,20,20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('ARUN',
                        style: TextStyle(
                            color: Color.fromARGB(255, 175, 175, 175),
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 10)),
                    SizedBox(
                      height: 10,
                    ),
                    Text( "Welcome to Kutumba's Digital Music archive! All our 7 albums  are online. This festive season we would like to share our 16 years of hard work and more to come, on our very own platform",
                        style: TextStyle(
                            color: Color.fromARGB(255, 175, 175, 175),
                            fontSize: 16,
                            height: 1.4))
                  ],
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset('assets/images/img2.png'),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('ARUN',
                        style: TextStyle(
                            color: Color.fromARGB(255, 175, 175, 175),
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 10)),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                        "Welcome to Kutumba's Digital Music archive! All our 7 albums  are online.",
                        style: TextStyle(
                            color: Color.fromARGB(255, 175, 175, 175),
                            fontSize: 16,
                            height: 1.4))
                  ],
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset('assets/images/img3.png'),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('ARUN',
                        style: TextStyle(
                            color: Color.fromARGB(255, 175, 175, 175),
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 10)),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                        "Welcome to Kutumba's Digital Music archive! All our 7 albums  are online.",
                        style: TextStyle(
                            color: Color.fromARGB(255, 175, 175, 175),
                            fontSize: 16,
                            height: 1.4))
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
