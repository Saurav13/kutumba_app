import 'package:flutter/material.dart';

import './../main_drawer.dart';
import './../components/header_logo.dart';

class Profile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Profile();
  }
}

class _Profile extends State<Profile> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.black12,
      endDrawer: MainDrawer(),
      appBar: AppBar(
        backgroundColor: Colors.black12,
        // Here we take the value from the MyProfilePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: HeaderLogo(),
        centerTitle: false,
      ),
      body: ListView(
        children: [
          Card(
            margin: EdgeInsets.all(20),
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(),
                      Text(
                        'MY PROFILE',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w300,
                          // fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 200, 200, 200),
                        ),
                      ),
                      Icon(
                        Icons.person_add_alt_1,
                        color: Color.fromARGB(255, 232, 122, 82),
                        size: 18,
                      )
                    ],
                  ),
                  Divider(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Name",
                          style: TextStyle(
                              height: 1,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              color: Color.fromARGB(255, 230, 230, 230))),
                      TextField(
                        decoration: InputDecoration(
                            // border: const OutlineInputBorder(),
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 15,
                                color: Colors.white),
                            hintText: 'Saurav Shrestha'),
                      )
                    ],
                  ),
                  SizedBox(height:20),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text("Address",
                          style: TextStyle(
                              height: 1,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              color: Color.fromARGB(255, 230, 230, 230))),
                      TextField(
                        decoration: InputDecoration(
                            // border: const OutlineInputBorder(),
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 15,
                                color: Colors.white),
                            hintText: 'Enter a search term'),
                      )
                    ],
                  ),
                  SizedBox(height:20),
                 
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text("Email",
                          style: TextStyle(
                              height: 1,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              color: Color.fromARGB(255, 230, 230, 230))),
                      TextField(
                        decoration: InputDecoration(
                            // border: const OutlineInputBorder(),
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 15,
                                color: Colors.white),
                            hintText: 'Enter a search term'),
                      )
                    ],
                  ),
                  SizedBox(height:10),

                  RaisedButton(
                    color: Color.fromARGB(255, 33, 33, 33),
                    onPressed: (){},
                    child: Text("Save"),
                  ),
                  SizedBox(height:10),
                  Divider(),
                  SizedBox(height:10),



                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text("Password",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              height: 1,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              color: Color.fromARGB(255, 230, 230, 230))),
                      SizedBox(height: 12),
                      Row(
                        children: [
                          RaisedButton(
                            color: Color.fromARGB(255, 33, 33, 33),
                            onPressed: () {},
                            child: Text("Change Password"),
                          ),
                        ],
                      )
                    ],
                  ),

                          SizedBox(height:10),
                  Divider(),
                  SizedBox(height:10),
                  
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text("Subscription",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              height: 1,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              color: Color.fromARGB(255, 230, 230, 230))),
                      SizedBox(height: 10),
                      Text("Subscription till: 07 October, 2009 ",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              height: 1,
                              fontWeight: FontWeight.w300,
                              fontSize: 15,
                              color: Color.fromARGB(255, 230, 230, 230))),
                      SizedBox(height: 12),
                      Row(children: [
                        RaisedButton(
                          color: Color.fromARGB(255, 33, 33, 33),
                          onPressed: () {},
                          child: Text("Renew"),
                        ),
                      ])
                    ],
                  ),
                  SizedBox(height:20),
                  
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
