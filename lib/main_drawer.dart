import 'package:flutter/material.dart';

import './components/nav_items.dart';

class MainDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MainDrawerState();
  }
}

class _MainDrawerState extends State<MainDrawer> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Map styles = {
    'nav_item_text': {'fontSize': 16}
  };

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: Container(
        decoration: BoxDecoration(color: Colors.black12),
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              padding: EdgeInsets.zero,
              margin: EdgeInsets.zero,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/avatar.png',
                    height: 80,
                    width: 80,
                  ),
                  Text("Saurav Shrestha", style: TextStyle(fontSize: 17))
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.black38,
              ),
            ),
            Column(
              
              children: [
               
                ListTile(
                  title: NavItem('Albums', Icons.album),
                  onTap: () {
                    Navigator.of(context).popUntil(ModalRoute.withName('/'));          
                    Navigator.of(context).pushNamed('/albums');
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: NavItem('Videos', Icons.video_library),
                  onTap: () {
                    Navigator.of(context).popUntil(ModalRoute.withName('/'));
                    Navigator.of(context).pushNamed( '/videos');
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: NavItem('Profile', Icons.account_circle),
                  onTap: () {
                    Navigator.of(context).popUntil(ModalRoute.withName('/'));
                    Navigator.of(context).pushNamed( '/profile');
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: NavItem('Log Out', Icons.login),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
