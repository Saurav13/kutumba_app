import 'package:flutter/material.dart';

class NavItem extends StatelessWidget {

  final String nav_text;
  final IconData icon;

  NavItem(this.nav_text,this.icon);

  @override
  Widget build(BuildContext context) {
    return Row(
      
      children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Icon(icon),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(nav_text,style: TextStyle(fontSize: 16),),
      ),
    ]);
  }
}